# Piano-Monogame

## Demo

https://www.youtube.com/watch?v=g-xmYvOMZzA

## Description

A MonoGame Application where you can play the Piano

## Technology Stack

- C#
- MonoGame
- GIMP

## How to Run

Open the Piano-MonoGame project folder in your preferred IDE

1. Go to the project folder .\InteractivePiano\
2. Run the command 'dotnet run' in your terminal

## Usage

Once the application starts, you will see a Piano

To play the piano simply press any key that is written on screen
To exit out of the application press 'esc' key or the 'x' to close the window