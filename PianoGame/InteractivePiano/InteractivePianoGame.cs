﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PianoSimulation;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using System;

namespace InteractivePiano
{
    public class InteractivePianoGame : Game
    {
        // Const/Readonly Field
        private const int TheSamplingRate = 44100;
        private const int BlackOffset = 10;
        private const int WhiteOffset = 40;
        private readonly Vector2 AddVector = new Vector2(0, 100);
        private readonly Vector2 ExitVector = new Vector2(450, 400);
        private readonly Vector2 KeyBoardVector = new Vector2(0, 300);

        // MonoGame Fields
        private Texture2D _blackKey;
        private Texture2D _whiteKey;
        private Texture2D _blackKeyPressed;
        private Texture2D _whiteKeyPressed;
        private SpriteFont _arialFont;
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        
        // Fields
        private Piano _piano;
        private IDictionary<char, Key> _keyNotes = new Dictionary<char, Key>();

        // Constructor InteractivePianoGame
        public InteractivePianoGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            _piano = new Piano();
        }

        // Maps the dictionary with note to key object.
        private void SetNotes(){
            int whiteIndex = 0;
            int noteTracker = 0;

            for (int i = 0; i < _piano.Keys.Length; i++){
                // White Keys
                if (Key.Notes[noteTracker].Length != 2){
                    _keyNotes.Add(_piano.Keys[i], new WhiteKey(Key.Notes[noteTracker], new Vector2(whiteIndex * WhiteOffset, 0)));
                    whiteIndex++;
                }
                // Black Keys
                else{
                    _keyNotes.Add(_piano.Keys[i], new BlackKey(Key.Notes[noteTracker],new Vector2(whiteIndex * WhiteOffset - BlackOffset, 0)));
                }

                noteTracker++;
                if (noteTracker == Key.Notes.Length){
                    noteTracker = 0;
                }
            }
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            SetNotes();
            _graphics.PreferredBackBufferWidth = 900;
            _graphics.PreferredBackBufferHeight = 500;
            _graphics.ApplyChanges();
            base.Initialize();
        }

        // Loads all the assets for the LoadContent function.
        private void LoadAsset(){
            _blackKey = Content.Load<Texture2D>("BlackKey");
            _whiteKey = Content.Load<Texture2D>("WhiteKey");
            _blackKeyPressed = Content.Load<Texture2D>("BlackKeyPressed");
            _whiteKeyPressed = Content.Load<Texture2D>("WhiteKeyPressed");
            _arialFont = Content.Load<SpriteFont>("File");
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here
            LoadAsset();
        }

        // Queue the note to be played then play the piano.
        private void PlayNote(char key){
            try{
                Task.Run(() => {
                    _piano.StrikeKey(key);
                    // Play the piano
                    Audio.Instance.Reset();
                    for (int i = 0; i < 3 * TheSamplingRate; i++){
                        Audio.Instance.Play(_piano.Play());
                    }
                });
                
                //PlayPiano();
            }
            catch (Exception e){
                Debug.WriteLine(e.Message);
            }
        }
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            // Check every key press that is not a special character
            foreach (Keys k in Keyboard.GetState().GetPressedKeys()){
                if (Key.AlphabetKeys.Contains(k.ToString().ToLower())){
                    PlayNote(k.ToString().ToLower()[0]);
                }
            }

            // Check every key press that is a special character
            foreach (Keys k in Keyboard.GetState().GetPressedKeys()){
                if (Key.SpecialKeys.Contains(k.ToString())){
                    PlayNote(Key.ConvertSpecialKeyToNote(k.ToString()));
                }
            }
 
            // Exit Program
            if (Keyboard.GetState().IsKeyDown(Keys.Escape)){
                Audio.Instance.Dispose();
                Exit();
            }
            base.Update(gameTime);
        }


        // Draw the black highlight and the note corresponding to the keyboard press.
        private void DrawBlackNote(char key){
            try{
                _spriteBatch.Draw(_blackKeyPressed, _keyNotes[key].Position, Color.White);
                _spriteBatch.DrawString(_arialFont, _keyNotes[key].Note.ToString(), _keyNotes[key].Position + AddVector, Color.White);
            }
            catch (Exception e){
               Debug.WriteLine(e.Message);
            }
        }

        // Draw the white highlight and the note corresponding to the keyboard press.
        private void DrawWhiteNote(char key){
            try{
                _spriteBatch.Draw(_whiteKeyPressed, _keyNotes[key].Position, Color.White);
                _spriteBatch.DrawString(_arialFont, _keyNotes[key].Note.ToString(), _keyNotes[key].Position + AddVector, Color.White);
            }
            catch (Exception e){
                Debug.WriteLine(e.Message);
            }
        }

        // Draw the piano on the GUI.
        private void DrawPiano(){
            
            // Draw White keys
            foreach (Key k in _keyNotes.Values){
                if (k.GetType() == typeof(WhiteKey)){
                    _spriteBatch.Draw(_whiteKey, k.Position, Color.White);   
                }
            }

            // Draw Highlight on white keys being pressed
            // Check every key press and compare with white keys.
            foreach (Keys k in Keyboard.GetState().GetPressedKeys()){
                // Checks to see if its a white key alphabet.
                if (WhiteKey.AlphabetWhiteKeys.Contains(k.ToString().ToLower())){
                    DrawWhiteNote(k.ToString().ToLower()[0]);
                }
                // Checks to see if its white key special
                if (WhiteKey.SpecialWhiteKeys.Contains(k.ToString())){
                    DrawWhiteNote(Key.ConvertSpecialKeyToNote(k.ToString()));
                }
            }

            // Draw Black Keys
            foreach (Key k in _keyNotes.Values){
                if (k.GetType() == typeof(BlackKey)){
                    _spriteBatch.Draw(_blackKey, k.Position, Color.White); 
                }
            }

            // Draw Highlights on black key being pressed
            // Check every key press and compare with white keys.
            foreach (Keys k in Keyboard.GetState().GetPressedKeys()){
                // Checks to see if its a white key alphabet.
                if (BlackKey.AlphabetBlackKeys.Contains(k.ToString().ToLower())){
                    DrawBlackNote(k.ToString().ToLower()[0]);
                }
                // Checks to see if its white key special
                if (BlackKey.SpecialBlackKeys.Contains(k.ToString())){
                    DrawBlackNote(Key.ConvertSpecialKeyToNote(k.ToString()));
                }
            }
        }

        // The order is this way for the sprite to appear correctly on top of the others.
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            _spriteBatch.Begin();

            DrawPiano();
        
            // How to exit the program
            _spriteBatch.DrawString(_arialFont, "Press ESC key to exit the program.", ExitVector, Color.White);

            // Show the playable notes.
            _spriteBatch.DrawString(_arialFont, "Playable Keys: " + _piano.Keys, new Vector2(0, 250), Color.White);

            // To see which keyboard note was played.
            foreach (Keys k in Keyboard.GetState().GetPressedKeys()){
                _spriteBatch.DrawString(_arialFont,"Keyboard Note: " + k.ToString(), KeyBoardVector, Color.White);
            }

            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
