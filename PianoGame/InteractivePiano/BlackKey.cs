using Microsoft.Xna.Framework;

namespace InteractivePiano{
    public class BlackKey: Key{

        // Static properties
        public static string AlphabetBlackKeys{
            get { return "dfgjk"; }
        }
        public static string SpecialBlackKeys{ 
            get{ return "D2D4D5D7D8D9OemMinusOemPlusOemSemicolonOemQuotes"; } 
        } 

        // Constructor BlackKey
        public BlackKey(string note, Vector2 position) : base(note, position) {

        }
    }
}