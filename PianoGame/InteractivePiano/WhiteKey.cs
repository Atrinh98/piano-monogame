using Microsoft.Xna.Framework;

namespace InteractivePiano{
    public class WhiteKey: Key{
        
        // Static properties
        public static string AlphabetWhiteKeys{
            get { return "qwertyuiopzxcvbnm"; }
        }
        public static string SpecialWhiteKeys{ 
            get{ return "OemOpenBracketsOemCommaOemPeriodOemQuestionSpace"; } 
        } 

        // Constructor WhiteKey
        public WhiteKey(string note, Vector2 position) : base(note, position) {

        }
    }
}