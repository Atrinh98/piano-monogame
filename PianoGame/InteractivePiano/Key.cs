using Microsoft.Xna.Framework;

namespace InteractivePiano{
    public abstract class Key{
        
        // Static properties
        public static string[] Notes { 
            get{ return new string[12] {"A", "A#", "B", "C", "C#", "D","D#", "E", "F", "F#", "G", "G#"}; }
        }
        public static string AlphabetKeys {
            get { return "qwertyuiopzxdcfvgbnjmk";}
        }
        public static string SpecialKeys {
            get { return "OemOpenBracketsOemCommaOemPeriodOemQuestionSpaceD2D4D5D7D8D9OemMinusOemPlusOemSemicolonOemQuotes"; }
        }

        //Properties for Key
        public string Note{get;}
        public Vector2 Position{get;}

        // Constructor Key
        public Key(string note, Vector2 position){
            Note = note;
            Position = position;
        }

        // Convert special keyboard keys to notes.
        public static char ConvertSpecialKeyToNote(string key){
            // 2
            if (key.Equals("D2")){
                return '2';
            }
            // 4
            else if (key.Equals("D4")){
                return '4';
            }
            // 5
            else if (key.Equals("D5")){
                return '5';
            }
            // 7
            else if (key.Equals("D7")){
                return '7';
            }
            // 8
            else if (key.Equals("D8")){
                return '8';
            }
            // 9
            else if (key.Equals("D9")){
                return '9';
            }
            // -
            else if (key.Equals("OemMinus")){
                return '-';
            }
            // =
            else if (key.Equals("OemPlus")){
                return '=';
            }
            // ;
            else if (key.Equals("OemSemicolon")){
                return ';';
            }
            // \'
            else if (key.Equals("OemQuotes")){
                return '\'';
            }
            // [
            else if (key.Equals("OemOpenBrackets")){
                return '[';
            }
            // ,
            else if (key.Equals("OemComma")){
                return ',';
            }
            // .
            else if (key.Equals("OemPeriod")){
                return '.';
            }
            // /
            else if (key.Equals("OemQuestion")){
                return '/';
            }
            // space
            else if (key.Equals("Space")){
                return ' ';
            }
            return '\0';
        }   
    }
}