using System;
using System.Collections.Generic;

namespace PianoSimulation
{
    public class Piano : IPiano
    {   
        //Fields
        private List<PianoWire> _wires;

        /// <summary>
        /// Lists all the characters associated with a key
        /// </summary>
        /// <value></value>
        public string Keys {
            get;
        }

        private int _samplingRate;

        // Constructor
        public Piano(string keys = "q2we4r5ty7u8i9op-[=zxdcfvgbnjmk,.;/' ",  int samplingRate = 44100){
            _wires = new List<PianoWire>();
            Keys = keys;
            _samplingRate = samplingRate;
            FillWireList();
            FillCircularArray();
        }

        /// <summary>
        /// Strikes the piano key (wire) corresponding to the specified character
        /// </summary>
        /// <param name="key">The charcter associated with a note</param>
        public void StrikeKey(char key){
            
            int Note = Keys.IndexOf(key);

            // Throw error if its -1.
            if (Note == -1){
                throw new ArgumentNullException("Did not find the key.");
            }
            _wires[Note].Strike();
        }

        /// <summary>
        /// Plays all of the vibrating keys (wires) at the current time step.
        /// </summary>
        /// <returns>Returns the combined harmonic result.</returns>
        public double Play(){
            double Samples = 0;

            for (int i = 0; i < _wires.Count; i++){
                Samples += _wires[i].Sample();
            }

            return Samples;
        }

        /// <summary>
        /// List containing a string descibring all the wires 
        /// in the piano with their key and note frequency
        /// </summary>
        /// <returns></returns>
        public List<string> GetPianoKeys(){

            List<string> Wires = new List<string>();

            for (int i = 0; i < Keys.Length; i++){
                Wires.Add("Key: '" + Keys[i] + "', Frequency: " + _wires[i].NoteFrequency + "\n");//CalculateFrequency(i) + "\n");
            }

            return Wires;
        }

        
        // Fills the WireList with wires.
        private void FillWireList(){
            for (int i = 0; i < Keys.Length; i++){
                _wires.Add(new PianoWire(CalculateFrequency(i), _samplingRate));
            }
        }

        private void FillCircularArray(){
            for (int i = 0; i < _wires.Count; i++){
                _wires[i].Strike();
            }
        }

        // Calculate the frequency depending on n.
        private double CalculateFrequency(int n){
            return Math.Pow(2,((n-24)/12.0)) * 440;
        }
    }
}