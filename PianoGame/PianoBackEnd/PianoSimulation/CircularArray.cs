using System;

namespace PianoSimulation
{
    public class CircularArray : IRingBuffer
    {
        //Fields
        private double[] _buffer;
        private int _bufferTracker;

        // Constructor
        public CircularArray(int bufferLength){
            // Validation
            if (bufferLength < 0){
                throw new ArgumentException("Cannot be lower than 0");
            }
            _buffer = new double[bufferLength];
            _bufferTracker = 0;
        }

        /// <summary>
        /// Length of the buffer
        /// </summary>
        public int Length{ 
            get { return _buffer.Length; }  
        }

        /// <summary>
        /// Track where the index is in the circular array.
        /// </summary
        public int Tracker{
            get { return _bufferTracker; }
        }

        /// <summary>
        /// Returns and removes the first element in the buffer. Adds value to the end of the buffer
        /// </summary>
        /// <param name="value">Value to be added at the end of the buffer</param>
        /// <returns>First element in the buffer</returns>
        public double Shift(double value){

            // Saves the value to return.
            double first = _buffer[_bufferTracker];
            // Overwrite the last element to value. 
            _buffer[_bufferTracker] = value;

            // Increment the tracker by one.
            _bufferTracker++;
            // Validation.
            if (_bufferTracker == _buffer.Length){
                _bufferTracker = 0;
            }
            
            // Return the first element.
            return first;
        }

        /// <summary>
        /// Indexer to go through elements in the buffer starting at the front to the value at the end
        /// </summary>
        /// <param name="index">index, where 0 indicates front of the ring buffer</param>
        /// <returns>element at the index</returns>
        public double this[int index]{ 
            get {
                return _buffer[index]; 
            }
        }

        /// <summary>
        /// Performs a deep copy of the array into the buffer
        /// </summary>
        /// <param name="array">array of doubles to be copied</param>
        public void Fill(double[] array){
            Array.Copy(array, _buffer, _buffer.Length);
        }
    }
}
